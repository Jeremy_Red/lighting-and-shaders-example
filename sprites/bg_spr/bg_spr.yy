{
    "id": "2391c298-e887-4904-867b-f370db0bfd73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fe04468-909f-4a69-b763-1d7b5e759010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2391c298-e887-4904-867b-f370db0bfd73",
            "compositeImage": {
                "id": "857f8537-ae86-443d-b851-a700e13e0a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fe04468-909f-4a69-b763-1d7b5e759010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "881855ba-d9f6-4cca-9723-e8877eb450f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fe04468-909f-4a69-b763-1d7b5e759010",
                    "LayerId": "6844d7dc-0187-4552-bcdd-6e924f4a1609"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "6844d7dc-0187-4552-bcdd-6e924f4a1609",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2391c298-e887-4904-867b-f370db0bfd73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}