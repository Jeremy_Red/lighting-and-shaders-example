{
    "id": "f043b0bb-c7a3-4f47-aee9-f2033896b2ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1f56bd6-92f9-467e-a250-a625c98b1c12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f043b0bb-c7a3-4f47-aee9-f2033896b2ee",
            "compositeImage": {
                "id": "e941bcd3-b95d-47b3-b2b2-91dff453cd36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1f56bd6-92f9-467e-a250-a625c98b1c12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c1c7a4-d7a7-41f2-9b03-fa9f05f9001f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1f56bd6-92f9-467e-a250-a625c98b1c12",
                    "LayerId": "3e758b98-b869-4523-a276-551a4ca35f9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "3e758b98-b869-4523-a276-551a4ca35f9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f043b0bb-c7a3-4f47-aee9-f2033896b2ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}