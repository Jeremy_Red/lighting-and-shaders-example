{
    "id": "6b408d6c-1022-4e58-977e-2e83a99222e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "greenbox_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3400f423-dffc-457e-bf0d-d57d6cf53e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b408d6c-1022-4e58-977e-2e83a99222e0",
            "compositeImage": {
                "id": "95627e13-c478-4a97-bed3-686686477873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3400f423-dffc-457e-bf0d-d57d6cf53e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96ae1545-2317-49bd-91a1-f4505be988d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3400f423-dffc-457e-bf0d-d57d6cf53e7f",
                    "LayerId": "1799c052-d710-48a3-b85e-2170c44c43ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1799c052-d710-48a3-b85e-2170c44c43ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b408d6c-1022-4e58-977e-2e83a99222e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}