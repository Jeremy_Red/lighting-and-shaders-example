{
    "id": "f309caad-2d6f-4082-b36e-0905ebe67087",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jojo_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 449,
    "bbox_left": 20,
    "bbox_right": 311,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f78afb1-d6b1-41a7-8ac4-7729bc342e89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f309caad-2d6f-4082-b36e-0905ebe67087",
            "compositeImage": {
                "id": "bb0d06ef-5026-48b5-a174-8cf68c7d5ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f78afb1-d6b1-41a7-8ac4-7729bc342e89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "810e75ee-8e9f-42e6-bb65-2104ff03e5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f78afb1-d6b1-41a7-8ac4-7729bc342e89",
                    "LayerId": "251ee172-9077-4aa3-8319-404236e3e807"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 450,
    "layers": [
        {
            "id": "251ee172-9077-4aa3-8319-404236e3e807",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f309caad-2d6f-4082-b36e-0905ebe67087",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 316,
    "xorig": 0,
    "yorig": 0
}