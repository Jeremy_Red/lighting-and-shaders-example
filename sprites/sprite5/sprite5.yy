{
    "id": "0041fd05-5db0-4805-9eb4-25d3bd018e87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35cf9408-6580-4123-a56f-18670733a9fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0041fd05-5db0-4805-9eb4-25d3bd018e87",
            "compositeImage": {
                "id": "94de6ee3-cb6b-4cb8-b260-7619031b5e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35cf9408-6580-4123-a56f-18670733a9fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e90dca-8dab-4952-a3b8-691c8ef89a78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35cf9408-6580-4123-a56f-18670733a9fb",
                    "LayerId": "25f08629-5d20-49dd-a6e6-320360412401"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "25f08629-5d20-49dd-a6e6-320360412401",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0041fd05-5db0-4805-9eb4-25d3bd018e87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}