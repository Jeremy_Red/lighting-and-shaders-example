{
    "id": "d37e8787-341a-4819-97ee-d1f55317499c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "greenbox",
    "eventList": [
        {
            "id": "ef874411-8072-4853-9e6d-914dbc802f5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d37e8787-341a-4819-97ee-d1f55317499c"
        },
        {
            "id": "50a8f2de-3552-442b-8a7a-be3e3b4399d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "d37e8787-341a-4819-97ee-d1f55317499c"
        },
        {
            "id": "41b327f9-1c4b-459e-9c05-aad42f4aa7d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "d37e8787-341a-4819-97ee-d1f55317499c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "6b408d6c-1022-4e58-977e-2e83a99222e0",
    "visible": true
}