{
    "id": "b14e1818-468e-419f-9d92-f2d5517b7e92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "light_renderer",
    "eventList": [
        {
            "id": "d51016cf-1f93-4f79-a31b-9b64de854194",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b14e1818-468e-419f-9d92-f2d5517b7e92"
        },
        {
            "id": "93b87476-36ca-43ff-8217-ef9e65bc9385",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b14e1818-468e-419f-9d92-f2d5517b7e92"
        },
        {
            "id": "db9419e0-16e1-4b08-8427-48a532aed274",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "b14e1818-468e-419f-9d92-f2d5517b7e92"
        },
        {
            "id": "14583a82-aa61-427f-ae1e-f89c4f415e5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b14e1818-468e-419f-9d92-f2d5517b7e92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}