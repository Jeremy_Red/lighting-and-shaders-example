/// @description Create the light surface
// You can write your code in this editor
depth = -100
camx = 0;
camy = 0;
global.light_surface = surface_create(1024,768);
global.base_surface = surface_create(1024,768);
global.final_surface = surface_create(1024,768);
ambientParam = shader_get_uniform(lighting,"ambientLightColor");
lightsTexture = shader_get_sampler_index(lighting,"lightsTexture");
baseTexture = shader_get_sampler_index(lighting,"baseTexture");
