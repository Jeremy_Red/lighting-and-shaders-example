/// @description Insert description here
// You can write your code in this editor

shader_set(lighting);
texture_set_stage(baseTexture,surface_get_texture(global.base_surface));
texture_set_stage(lightsTexture,surface_get_texture(global.light_surface));
shader_set_uniform_f(ambientParam,0.25,0.25,0.25,1);
surface_set_target(global.final_surface);
draw_surface(global.final_surface,0,0);
surface_reset_target();
shader_reset()
draw_surface(global.final_surface,0,0);
surface_set_target(global.base_surface);
draw_clear_alpha(c_white,0);
surface_reset_target();
surface_set_target(global.light_surface);
draw_clear_alpha(c_red,0);
surface_reset_target();
surface_set_target(global.final_surface);
draw_clear_alpha(c_white,0);
surface_reset_target();