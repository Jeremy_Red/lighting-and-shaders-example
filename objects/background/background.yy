{
    "id": "e7da0211-244f-47db-9240-fecd6b6a08eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "background",
    "eventList": [
        {
            "id": "d122a198-d565-4fb9-bb25-39351c98b65e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7da0211-244f-47db-9240-fecd6b6a08eb"
        },
        {
            "id": "123512a2-6e99-4596-a1ea-0902841ad7a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e7da0211-244f-47db-9240-fecd6b6a08eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2391c298-e887-4904-867b-f370db0bfd73",
    "visible": true
}