{
    "id": "2d65cfac-1155-478f-b5b1-468daa3ee505",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player",
    "eventList": [
        {
            "id": "58272829-0665-4a38-9d23-60c240971f8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "2d65cfac-1155-478f-b5b1-468daa3ee505"
        },
        {
            "id": "e5b4b35e-e4a0-49e0-ad4a-4a1be97ca941",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "2d65cfac-1155-478f-b5b1-468daa3ee505"
        },
        {
            "id": "358635c9-e5fb-4e4d-8729-ae8a03e3f517",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "2d65cfac-1155-478f-b5b1-468daa3ee505"
        },
        {
            "id": "09694f70-a3b5-4eb8-9435-8dabb6d7c445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "2d65cfac-1155-478f-b5b1-468daa3ee505"
        },
        {
            "id": "eecf7f3e-5d0b-4a14-bd53-129349762e6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2d65cfac-1155-478f-b5b1-468daa3ee505"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b408d6c-1022-4e58-977e-2e83a99222e0",
    "visible": true
}