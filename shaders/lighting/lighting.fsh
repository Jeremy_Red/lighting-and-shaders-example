//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform sampler2D lightsTexture;
uniform sampler2D baseTexture;
uniform vec4 ambientLightColor;

void main()
{
	vec4 lightValue = texture2D(lightsTexture,v_vTexcoord);
	vec4 baseColor = texture2D(baseTexture,v_vTexcoord);
    //gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord )*vec4(1,1,1,1);
	gl_FragColor = (baseColor*ambientLightColor)+(baseColor*(
	vec4(1,1,1,1)-ambientLightColor)*lightValue.a);
}